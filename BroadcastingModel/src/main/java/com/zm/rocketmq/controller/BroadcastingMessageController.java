package com.zm.rocketmq.controller;

import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/broadcasting")
public class BroadcastingMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    //  http://localhost:8001/broadcasting/send/1111
    @RequestMapping("/send/{msg}")
    public void broadcastingMesgSend(@PathVariable(value = "msg") String msg){

        rocketMQTemplate.asyncSend("modeTopic", msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.err.println("消息发送成功");
            }

            @Override
            public void onException(Throwable throwable) {
                System.err.println("消息发送失败，失败原因为："+throwable.getMessage());
            }
        });

    }
}
