package com.zm.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = "modeTopic",
        consumerGroup = "mode-consumer-group-b",
        messageModel = MessageModel.BROADCASTING //广播模式
       )
public class BroadcastingListener3 implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        System.err.println("我是mode-consumer-group-b组的第三个消费者"+s);
    }
}
