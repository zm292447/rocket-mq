package com.zm.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 广播模式下 ， 消息会被每一个消费者都处理一次 ， mq服务器不会记录消费位点 ， 也不会重试
 */
@Component
@RocketMQMessageListener(topic = "modeTopic",
        consumerGroup = "mode-consumer-group-b",
        messageModel = MessageModel.BROADCASTING //广播模式 ， 消费位点mq不回去移动，不回去却热播消息是否被消费，也不会去重试
       )
public class BroadcastingListener1 implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        System.err.println("我是mode-consumer-group-b组的第一个消费者"+s);
    }
}
