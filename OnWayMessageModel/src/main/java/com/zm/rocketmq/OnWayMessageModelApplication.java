package com.zm.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnWayMessageModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnWayMessageModelApplication.class, args);
    }

}
