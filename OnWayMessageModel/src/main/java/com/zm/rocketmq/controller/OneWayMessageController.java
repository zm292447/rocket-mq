package com.zm.rocketmq.controller;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;


@RestController
@RequestMapping("/oneway")
public class OneWayMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;
    //  http://localhost:8001/oneway/send1/123222244444
    @RequestMapping("/send1/{msg}")
    public void onewayMegSend1(@PathVariable(value = "msg") String msg){
        rocketMQTemplate.sendOneWay("onewayTopic1",msg);
    }

    //  http://localhost:8001/oneway/send2/123222244444
    @RequestMapping("/send2/{msg}")
    public void onewayMegSend2(@PathVariable(value = "msg") String msg){
        Message<Order> message=new Message<Order>() {
            @Override
            public Order getPayload() {
                Order order=new Order();
                order.setId(1L);
                order.setOrderNo(String.valueOf(UUID.randomUUID()));
                order.setPrice(9999L);
                order.setOrderTile("同步消息测试商品:"+msg);
                order.setAddTime(System.currentTimeMillis());
                return order;
            }

            @Override
            public MessageHeaders getHeaders() {
                return null;
            }
        };

        rocketMQTemplate.sendOneWay("onewayTopic2",message);
    }
}
