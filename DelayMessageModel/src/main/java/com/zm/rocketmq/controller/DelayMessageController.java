package com.zm.rocketmq.controller;


import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/delay")
public class DelayMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    //   http://localhost:8001/delay/send/1232222
    @RequestMapping("/send/{msg}")
    public void delayMegSend(@PathVariable(value = "msg") String msg){
        Message<String> message= MessageBuilder.withPayload(msg).build();
        //                                 主题            消息体        超时时间        延迟等级
        rocketMQTemplate.syncSend("delayTopic",message,3000,3);
    }
}
