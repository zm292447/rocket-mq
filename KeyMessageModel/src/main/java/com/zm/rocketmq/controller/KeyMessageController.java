package com.zm.rocketmq.controller;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/key")
public class KeyMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    // http://localhost:8001/key/send/123222244444
    @RequestMapping("/send/{msg}")
    public void syncSimpleMessage(@PathVariable(value = "msg") String msg){
//这里更改类型，消费者也要改监听到的类型
        Order order=  new Order(1L, "orderNo"+msg,"1",1L,System.currentTimeMillis());
        Message<Order> message= MessageBuilder.withPayload(order)
                        .setHeader(RocketMQHeaders.KEYS,"testKey")
                                .build();
        rocketMQTemplate.syncSend("keyTopic",message);
    }


}
