package com.zm.rocketmq.listener;

import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;


@Component
@RocketMQMessageListener(topic = "keyTopic",consumerGroup = "boot-consumer-group")
public class KeyMessageListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt m) {
        System.err.println("key :"+m.getKeys());
        System.out.println("msg body: "+m.getBody());
    }
}
