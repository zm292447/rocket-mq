package com.zm.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClusteringMessageModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClusteringMessageModelApplication.class, args);
    }

}
