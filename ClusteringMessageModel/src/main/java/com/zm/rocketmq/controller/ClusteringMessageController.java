package com.zm.rocketmq.controller;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/cluster")
public class ClusteringMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    //  http://localhost:8001/cluster/send/123
    //源码不建议超过1m ， 实际上是不能超过4m ， 1m性能最好
    @RequestMapping("/send/{msg}")
    public void syncSimpleMessage(@PathVariable(value = "msg") String msg){
        for (int i = 0; i < 10; i++) {
            rocketMQTemplate.syncSend("clusterTopic",msg+"_"+i);
        }


    }


}
