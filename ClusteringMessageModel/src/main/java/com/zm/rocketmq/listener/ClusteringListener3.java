package com.zm.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = "clusterTopic",
        consumerGroup = "mode-consumer-group-a",
        messageModel = MessageModel.CLUSTERING //集群模式 负载均衡
)
public class ClusteringListener3 implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        System.err.println("我是mode-consumer-group-a组的第三个消费者");
    }
}
