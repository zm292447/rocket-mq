package com.zm.rocketmq.listener;

import com.alibaba.fastjson.JSON;
import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = "orderTopic",
        consumerGroup = "boot-consumer-group",
        consumeMode = ConsumeMode.ORDERLY
)
public class OrderMessageListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt messageExt) {
        Order order = JSON.parseObject(new String(messageExt.getBody()), Order.class);
        System.err.println(order);
    }
}
