package com.zm.rocketmq.controller;

import com.alibaba.fastjson.JSON;
import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/order")
public class OrderMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    // http://localhost:8001/order/send/1111
    //消息会按照发送的消息排序
    @RequestMapping("/send/{msg}")
    public void orderMegSend(){
        String orderNo= UUID.randomUUID().toString();
        List<Order> orderList = Arrays.asList(
                new Order(1L, orderNo,"1",1L,System.currentTimeMillis()),
                new Order(2L, orderNo,"2",2L,System.currentTimeMillis()),
                new Order(3L, orderNo,"3",3L,System.currentTimeMillis()),
                new Order(4L, orderNo,"4",4L,System.currentTimeMillis()),
                new Order(5L, orderNo,"5",5L,System.currentTimeMillis())
        );
        orderList.forEach(order -> {
            //通常一json的方式处理消息                                                            按照订单号做运算，统一将所有消息发送到同一个队列中，队列是一个消息一个消息取得，所以在队列中有序，被消费时也有序
            rocketMQTemplate.syncSendOrderly("orderTopic", JSON.toJSONString(order),order.getOrderNo());
        });
    }

}
