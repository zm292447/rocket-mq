package com.zm.rocketmq.controller;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

@RestController
@RequestMapping("/async")
public class ConsumerTimeOutAsyncMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    //http://localhost:8001/async/send/qwe
    @RequestMapping("/send/{msg}")
    public void asyncMsgSend1(@PathVariable(value = "msg") String msg){
        //异步消息发送，新开辟一个线程去发送消息等待消息成功发送到mq的回调
        rocketMQTemplate.asyncSend("asyncTopic", msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.err.println("消息发送成功，回调成功");

            }
            @Override
            public void onException(Throwable throwable) {
                System.err.println("消息发送失败，回调成功，发送失败原因："+throwable.getMessage());
            }
        });
    }

}
