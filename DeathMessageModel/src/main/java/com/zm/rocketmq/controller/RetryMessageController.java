package com.zm.rocketmq.controller;

import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/retry")
public class RetryMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    // http://localhost:8001/retry/send/111111
    @RequestMapping("/send/{msg}")
    public void retrySimpleMessage(@PathVariable(value = "msg") String msg){
        System.err.println("发送成功");
        rocketMQTemplate.syncSend("retryDeathTopic", msg);
    }


}
