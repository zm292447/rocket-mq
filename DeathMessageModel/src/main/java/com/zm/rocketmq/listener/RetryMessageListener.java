package com.zm.rocketmq.listener;

import lombok.SneakyThrows;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

//重试间隔时长默认为延迟消息等级时长
@Component
@RocketMQMessageListener(topic = "retryDeathTopic",
        consumerGroup = "retryDeath-consumer-group",
        maxReconsumeTimes = 1
)
public class RetryMessageListener implements RocketMQListener<String> {
    int count=0;
    @SneakyThrows
    @Override
    public void onMessage(String s) {
        System.err.println("消费者接收到消息"+s);
        System.err.println("次数"+count++);

        //1抛出异常使其重试
        throw new Exception("retryTopic");
        //2 return null
        //3 返回RECONSUME_LATER

        //关于springboot +rocketmq 想要返回重试的话官方文档给出的方法啊是手动抛出异常出发重试机制
    }
}
