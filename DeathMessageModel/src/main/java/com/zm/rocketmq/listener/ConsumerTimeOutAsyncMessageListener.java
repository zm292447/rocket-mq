package com.zm.rocketmq.listener;

import lombok.SneakyThrows;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(
        topic = "asyncTopic",
        consumerGroup = "boot-consumer-group",
        consumeTimeout=1L
)
public class ConsumerTimeOutAsyncMessageListener implements RocketMQListener<String> {
    @SneakyThrows
    @Override
    public void onMessage(String s) {
        Thread.sleep(61000);
        System.err.println(s);
    }
}
