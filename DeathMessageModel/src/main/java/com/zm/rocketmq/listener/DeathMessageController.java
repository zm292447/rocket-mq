package com.zm.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = "%DLQ%retryDeath-consumer-group",
        consumerGroup = "retryDeath-consumer-group"
)
public class DeathMessageController implements RocketMQListener<Object> {
    @Override
    public void onMessage(Object o) {
        System.err.println("死信 "+o.toString());
    }
}
