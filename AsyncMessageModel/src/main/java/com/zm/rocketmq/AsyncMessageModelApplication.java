package com.zm.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsyncMessageModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsyncMessageModelApplication.class, args);
    }

}
