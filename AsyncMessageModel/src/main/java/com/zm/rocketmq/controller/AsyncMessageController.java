package com.zm.rocketmq.controller;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

@RestController
@RequestMapping("/async")
public class AsyncMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    //http://localhost:8001/async/send1/qwe
    @RequestMapping("/send1/{msg}")
    public void asyncMsgSend1(@PathVariable(value = "msg") String msg){
        //异步消息发送，新开辟一个线程去发送消息等待消息成功发送到mq的回调
        rocketMQTemplate.asyncSend("asyncTopic1", msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.err.println("消息发送成功，回调成功");

            }
            @Override
            public void onException(Throwable throwable) {
                System.err.println("消息发送失败，回调成功，发送失败原因："+throwable.getMessage());
            }
        });


    }
    // http://localhost:8001/async/send2/qwe
    @RequestMapping("/send2/{msg}")
    public void asyncMsgSend2(@PathVariable(value = "msg") String msg){
        Message<Order> message=new Message<Order>() {
            @Override
            public Order getPayload() {
                Order order=new Order();
                order.setId(1L);
                order.setOrderNo(String.valueOf(UUID.randomUUID()));
                order.setPrice(9999L);
                order.setOrderTile("异步消息测试商品:"+msg);
                order.setAddTime(System.currentTimeMillis());
                return order;
            }

            @Override
            public MessageHeaders getHeaders() {
                return null;
            }
        };
        rocketMQTemplate.asyncSend("asyncTopic2", message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.err.println("消息发送成功，回调成功");
            }
            @Override
            public void onException(Throwable throwable) {
                System.err.println("消息发送失败，回调成功，发送失败原因："+throwable.getMessage());
            }
        });

    }
}
