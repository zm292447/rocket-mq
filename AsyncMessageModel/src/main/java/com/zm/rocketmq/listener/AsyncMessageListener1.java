package com.zm.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = "asyncTopic1",consumerGroup = "boot-consumer-group")
public class AsyncMessageListener1 implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        System.err.println(s);
    }
}
