package com.zm.rocketmq.listener;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;


@Component
@RocketMQMessageListener(topic = "asyncTopic2",consumerGroup = "boot-consumer-group")
public class AsyncMessageListener2  implements RocketMQListener<Order> {
    @Override
    public void onMessage(Order s) {
        System.err.println(s.toString());
    }
}
