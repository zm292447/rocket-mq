package com.zm.rocketmq.controller;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 默认不支持sql这种方式，需要手动修改broker配置文件
 *  加一行 ： enablePropertyFilter=true
 */
@RestController
@RequestMapping("/sql")
public class SQLMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    // http://localhost:8001/sql/send/1111
    @RequestMapping("/send/{msg}")
    public void syncSimpleMessage(@PathVariable(value = "msg") String msg){
//        方式一
//        Map<String,Object> headers=new HashMap<>();
//        headers.put("key","sql");
//        headers.put("key2",2);
//        rocketMQTemplate.convertAndSend("sqlTopic",msg,headers);

//        方式二
        Message<String> msgs=new Message<String>() {
            @Override
            public String getPayload() {
                return msg;
            }

            @Override
            public MessageHeaders getHeaders() {
                Map<String,Object> headers=new HashMap<>();
                headers.put("key","sql");
                headers.put("key2",2);
                MessageHeaders headers1=new MessageHeaders(headers);
                return headers1;
            }
        };
        rocketMQTemplate.syncSend("sqlTopic",msgs);

    }
}
