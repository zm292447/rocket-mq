package com.zm.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 接收sql过滤规则的消息需要在注释中修改selectorType为SelectorType.SQL92
 * selectorExpression为过滤规则
 * selectorExpression的语法类似于sql语法where后面的部分，但是语法有限 ：
 * 数字比较 : > , < , >= , <= , between , =
 * 字符比较 ： = ， <> , in , is null , is not null
 * 逻辑运算符 ： and  , or , not
 */
@Component
@RocketMQMessageListener(topic = "sqlTopic",
        consumerGroup = "boot-consumer-group",
        selectorType = SelectorType.SQL92,
        selectorExpression = "key = 'sql' and key2 > 1")
public class SQLMessageListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        System.err.println(s);
    }
}
