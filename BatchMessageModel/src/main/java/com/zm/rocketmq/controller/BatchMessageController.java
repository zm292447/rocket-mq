package com.zm.rocketmq.controller;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/batch")
public class BatchMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    //  http://localhost:8001/batch/send
    @RequestMapping("/send")
    public void onewayMegSend(){
        List<Order> orderList = Arrays.asList(
                new Order(1L, UUID.randomUUID().toString(),"1",1L,System.currentTimeMillis()),
                new Order(2L, UUID.randomUUID().toString(),"2",2L,System.currentTimeMillis()),
                new Order(3L, UUID.randomUUID().toString(),"3",3L,System.currentTimeMillis()),
                new Order(4L, UUID.randomUUID().toString(),"4",4L,System.currentTimeMillis()),
                new Order(5L, UUID.randomUUID().toString(),"5",5L,System.currentTimeMillis())
        );
        rocketMQTemplate.syncSend("batchTopic",orderList);
    }
}
