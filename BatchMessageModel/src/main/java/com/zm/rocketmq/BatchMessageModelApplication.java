package com.zm.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchMessageModelApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchMessageModelApplication.class, args);
	}

}
