package com.zm.rocketmq.listener;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RocketMQMessageListener(topic = "batchTopic",consumerGroup = "boot-consumer-group")
public class BatchMessageListener implements RocketMQListener<List<Order>> {
    @Override
    public void onMessage(List<Order> orderList) {
        orderList.forEach(order -> {
            System.err.println(order.toString());
        });
    }
}
