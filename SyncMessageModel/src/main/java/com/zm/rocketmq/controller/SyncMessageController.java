package com.zm.rocketmq.controller;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

@RestController
@RequestMapping("/sync")
public class SyncMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @RequestMapping("/send/{msg}")
    public void syncSimpleMessage(@PathVariable(value = "msg") String msg){
//这里更改类型，消费者也要改监听到的类型
//        rocketMQTemplate.syncSend("syncTopic",msg);
        Message<Order> message=new Message<Order>() {
            @Override
            public Order getPayload() {
                Order order=new Order();
                order.setId(1L);
                order.setOrderNo(String.valueOf(UUID.randomUUID()));
                order.setPrice(9999L);
                order.setOrderTile("同步消息测试商品:"+msg);
                order.setAddTime(System.currentTimeMillis());
                return order;
            }

            @Override
            public MessageHeaders getHeaders() {
                return null;
            }
        };
        rocketMQTemplate.syncSend("syncTopic",message);
    }


}
