package com.zm.rocketmq.listener;

import com.boot.rocket.common.model.Order;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

//@Component
//@RocketMQMessageListener(topic = "syncTopic",consumerGroup = "boot-consumer-group")
//public class SyncMessageListener implements RocketMQListener<String> {
//    @Override
//    public void onMessage(String s) {
//        System.out.println(s);
//    }
//}
@Component
@RocketMQMessageListener(topic = "syncTopic",consumerGroup = "boot-consumer-group")
public class SyncMessageListener implements RocketMQListener<Order> {
    @Override
    public void onMessage(Order order) {
        System.out.println(order.toString());
    }
}
