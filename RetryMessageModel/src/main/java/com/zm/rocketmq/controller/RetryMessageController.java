package com.zm.rocketmq.controller;

import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/retry")
public class RetryMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    // http://localhost:8001/retry/send/123
    @RequestMapping("/send/{msg}")
    public void retrySimpleMessage(@PathVariable(value = "msg") String msg){
//这里更改类型，消费者也要改监听到的类型

        rocketMQTemplate.asyncSend("retryTopic", msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.err.println("发送成功");
            }

            @Override
            public void onException(Throwable throwable) {

            }
        });
//        Message<Order> message=new Message<Order>() {
//            @Override
//            public Order getPayload() {
//                Order order=new Order();
//                order.setId(1L);
//                order.setOrderNo(String.valueOf(UUID.randomUUID()));
//                order.setPrice(9999L);
//                order.setOrderTile("同步消息测试商品");
//                order.setAddTime(System.currentTimeMillis());
//                return order;
//            }
//
//            @Override
//            public MessageHeaders getHeaders() {
//                return null;
//            }
//        };
//        rocketMQTemplate.syncSend("syncTopic",message);
    }


}
