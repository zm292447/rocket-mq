package com.zm.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component          //死信队列的名称为%DLQ% + 消费者组名
@RocketMQMessageListener(topic = "%DLQ%boot-consumer-group",
        consumerGroup = "death-consumer-group"
)
public class DeathMessageController implements RocketMQListener<Object> {
    @Override
    public void onMessage(Object o) {
        System.err.println("死信 "+o.toString());
    }
}
