package com.zm.rocketmq.listener;

import lombok.SneakyThrows;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 当前为顺序消费模式，重试次数和间隔时间可自定义
 */
@Component
@RocketMQMessageListener(topic = "orderTopic",
        consumerGroup = "boot-consumer-group",
        consumeMode = ConsumeMode.ORDERLY,
        maxReconsumeTimes = 1

)
public class OrderMessageListener implements RocketMQListener<MessageExt> {
    int count=0;
    @SneakyThrows
    @Override
    public void onMessage(MessageExt messageExt) {
        System.err.println("消费者接收顺序到消息"+messageExt.getBody().toString());
        System.err.println("次数"+count++);
        throw new Exception("retryTopic");
//        Order order = JSON.parseObject(new String(messageExt.getBody()), Order.class);
//        System.err.println(order);
    }
}
