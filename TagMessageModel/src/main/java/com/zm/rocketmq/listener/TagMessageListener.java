package com.zm.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = "tagTopic",
        consumerGroup = "boot-consumer-group",
        selectorType = SelectorType.TAG,
        selectorExpression = "testTag || testTag2") //  ||是或的意思
public class TagMessageListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        System.err.println(s);
    }
}
