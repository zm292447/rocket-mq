package com.zm.rocketmq.controller;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/tag")
public class TagMessageController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    // http://localhost:8001/tag/send/qwe
    @RequestMapping("/send/{msg}")
    public void syncSimpleMessage(@PathVariable(value = "msg") String msg){
    //                                      主题：tag（最多只能有一个）
        rocketMQTemplate.syncSend("tagTopic:testTag",msg);

    }
}
