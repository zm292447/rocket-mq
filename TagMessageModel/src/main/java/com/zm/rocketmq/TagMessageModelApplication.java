package com.zm.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TagMessageModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(TagMessageModelApplication.class, args);
    }

}
